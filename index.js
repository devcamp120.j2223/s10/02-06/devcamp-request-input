// Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require("express");

// Khởi tạo app express
const app = express();

// Cấu hình để app đọc được body request dạng json
app.use(express.json());
// Cấu hình để app được được tiếng Việt UTF8
app.use(express.urlencoded({
    extended: true
}))

// Khai báo cổng của project
const port = 8000;

// Khai báo API dạng get "/" sẽ chạy vào đây
// Callback function: Là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/", (request, response) => {
    let today = new Date();

    // Khai bao class
    // Khoi tao 3 doi tuong
    // Tra ra mang dang JSON

    response.status(200).json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

app.get("/request-params/:param1", (request, response) => {
    let param1 = request.params.param1;

    response.status(200).json({
        param1: param1
    })
})

app.get("/request-query", (request, response) => {
    let query = request.query;

    response.status(200).json({
        queryRequest: query
    });
})

app.post("/request-body", (request, response) => {
    let body = request.body;

    response.status(200).json({
        bodyRequest: body
    })
})


// Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);
})